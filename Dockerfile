FROM docker:latest

RUN apk add make \
 && apk add mkcert --repository=https://dl-cdn.alpinelinux.org/alpine/edge/testing/ \
 && apk add rsync \
 && apk add yq \
 && apk add git
